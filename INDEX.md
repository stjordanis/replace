# REPLACE

Replaces files in the destination directory with files from the source directory that have the same name.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## REPLACE.LSM

<table>
<tr><td>title</td><td>REPLACE</td></tr>
<tr><td>version</td><td>1.2a</td></tr>
<tr><td>entered&nbsp;date</td><td>2001-03-25</td></tr>
<tr><td>description</td><td>Replaces files in the destination directory with files from the source directory that have the same name.</td></tr>
<tr><td>keywords</td><td>copy, replace</td></tr>
<tr><td>author</td><td>Rene Ableidinger &lt;rene.ableidinger -AT- gmx.at&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Rene Ableidinger &lt;rene.ableidinger -AT- gmx.at&gt;</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
